import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  Button
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import Axios from "axios";

import { saveToken } from "../redux/actions";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      categories: [],
      venues: []
    };
  }

  getVenue() {
    Axios({
      method: "GET",
      url: `https://mainbersama.demosanbercode.com/api/venues`,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${this.props.auth.token}`
      }
    })
      .then(res => {
        this.setState({ venues: res.data.venues });
      })
      .catch(err => console.log(err.message));
  }
  filterVenue(id) {
    Axios({
      method: "GET",
      url: `https://mainbersama.demosanbercode.com/api/venues`,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${this.props.auth.token}`
      }
    })
      .then(res => {
        this.setState({
          venues: res.data.venues.filter(e => e.category_id == id)
        });
      })
      .catch(err => console.log(err.message));
  }
  searchVenue(keyword) {
    Axios({
      method: "GET",
      url: `https://mainbersama.demosanbercode.com/api/venues`,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${this.props.auth.token}`
      }
    })
      .then(res => {
        console.log("search");

        // this.setState({
        //   venues: res.data.venues.filter(e => {
        //     let namaVenue = e.name.toLowerCase();
        //     console.log(namaVenue.search(keyword));
        //     return namaVenue.search(keyword) >= 0;
        //   })
        // });
      })
      .catch(err => console.log(err.message));
  }

  _retrieveData = async () => {
    try {
      let token = await AsyncStorage.getItem("token");
      this.setState({ token });
    } catch (error) {
      // Error retrieving data
      return "error";
    }
  };

  componentDidMount() {
    this._retrieveData();
    this.getVenue();
  }
  logout = () => {
    this.setState({ token: "" });
    AsyncStorage.removeItem("token");
    this.props.saveToken("");
  };

  render() {
    let token = this.state.token;
    return (
      <View style={styles.screen}>
        <Button title="Logout" onPress={() => this.logout()} />
        {/* <Button title="Logout" onPress={() => this.getVenue()} /> */}
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.searchRow}>
            <View style={styles.searchInput}>
              <TextInput
                style={styles.inputText}
                placeholder="Search..."
                placeholderTextColor="#718093"
                // onChangeText={text => this.searchVenue(text)}
              />
            </View>
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.kategoriText}>Kategori</Text>
            <ScrollView style={styles.cardKategoriScroll} horizontal={true}>
              {kategori.map((e, i) => {
                return (
                  <TouchableOpacity
                    style={styles.cardKategori}
                    key={i}
                    onPress={() => this.filterVenue(i + 1)}
                  >
                    <Ionicons name={e.icon} size={36} color={"#FFFFFF"} />
                    <Text style={styles.cardKategoriText}>{e.name}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.kategoriText}>Venue</Text>
            <ScrollView contentContainerStyle={styles.cardVenueScroll}>
              {this.state.venues.map((e, i) => {
                return (
                  <TouchableOpacity
                    style={styles.cardVenue}
                    onPress={() =>
                      this.props.navigation.navigate("Venue", {
                        id: e.id,
                        token
                      })
                    }
                    key={i}
                  >
                    <Image
                      source={{
                        uri:
                          "https://mainbersama.demosanbercode.com" + e.thumbnail
                      }}
                      style={{
                        height: "100%",
                        width: "30%",
                        resizeMode: "contain",
                        backgroundColor: "#ffffff",
                        borderRadius: 16,
                        flexDirection: "row"
                      }}
                    />
                    <View style={{ flexDirection: "column", marginLeft: 10 }}>
                      <Text style={{ color: "#0A3D62", fontSize: 16 }}>
                        {e.name}
                      </Text>
                      <Text style={{ color: "#0A3D62" }}>
                        Open at {e.open_hour}
                      </Text>
                      <Text style={{ color: "#0A3D62" }}>
                        (
                        {
                          ["Futsal", "Soccer", "Basket", "Voli"][
                            e.category_id - 1
                          ]
                        }
                        )
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    auth: state.authReducers
  };
};
const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

const kategori = [
  { name: "Futsal", icon: "ios-american-football" },
  { name: "Soccer", icon: "ios-football" },
  { name: "Basket", icon: "ios-basketball" },
  { name: "Voli", icon: "ios-baseball" }
];

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff"
  },
  searchRow: {
    alignItems: "center"
  },
  searchInput: {
    width: "90%",
    backgroundColor: "#F5F6FA",
    borderRadius: 25,
    height: 50,
    marginTop: 20,
    marginBottom: 10,
    justifyContent: "center",
    padding: 20,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowOffset: { height: 4, width: 4 }
  },
  inputText: {
    height: 50,
    color: "#718093"
  },
  kategoriText: {
    color: "#0A3D62",
    marginLeft: 20
  },
  cardKategoriScroll: {
    marginLeft: 10
  },
  cardKategori: {
    flexDirection: "column",
    alignContent: "center",
    alignItems: "center",
    padding: 15,
    margin: 5,
    height: 100,
    width: 100,
    backgroundColor: "#273C75",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardKategoriText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  },
  cardVenueScroll: {
    marginHorizontal: 10,
    alignItems: "center",
    flexDirection: "column"
  },
  cardVenue: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    height: 100,
    width: "100%",
    backgroundColor: "#F5F6FA",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardVenueText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  }
});
