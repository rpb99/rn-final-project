import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native";
import Logo from "../../assets/sport.png";
import { register } from '../redux/actions'
import { connect } from 'react-redux';


class Register extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      email: '',
      password: ''

    }
  }
  
      handleInput = data => {
        let obj = {
          [data.type] : data.text
        }
        this.setState(obj)
      }

      handleRegister = () => {
       this.props.register(this.state)
      }

  render() {
    console.log(this.state)
  return (
    <View style={styles.container}>
      <Text style={styles.logo}>Main Bersama</Text>
      <Image source={Logo} style={{ height: "25%", width: "80%" }} />
      {/* <View style={styles.inputView}>
        <TextInput
          secureTextEntry
          style={styles.inputText}
          placeholder="Name"
          placeholderTextColor="#718093"
          onChangeText={text => {}}
        />
      </View> */}
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          placeholder="Username"
          placeholderTextColor="#718093"
          onChangeText={text => this.handleInput({type: 'username', text})}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          placeholder="Email"
          placeholderTextColor="#718093"
          onChangeText={text => this.handleInput({type: 'email', text})}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          secureTextEntry
          style={styles.inputText}
          placeholder="Password"
          placeholderTextColor="#718093"
          onChangeText={text => this.handleInput({type: 'password', text})}
        />
      </View>
      <TouchableOpacity
        style={styles.signupBtn}
        onPress={() => this.handleRegister()}
      >
        <Text style={styles.loginText}>Register</Text>
      </TouchableOpacity>
    </View>
  );
}
}


const mapDispatchToProps = (dispatch) => {
  return {
    register: (data) => dispatch(register(data))
  }
}
export default connect(null, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    fontWeight: "bold",
    fontSize: 40,
    color: "#fb5b5a",
    marginBottom: 5
  },
  inputView: {
    width: "80%",
    backgroundColor: "#F5F6FA",
    borderRadius: 25,
    height: 50,
    marginBottom: 10,
    justifyContent: "center",
    padding: 20,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowOffset: { height: 4, width: 4 }
  },
  inputText: {
    height: 50,
    color: "#718093"
  },
  forgot: {
    color: "#718093",
    fontSize: 11
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#273C75",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText: {
    color: "white"
  },
  signupBtn: {
    width: "80%",
    backgroundColor: "#F6B93B",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
    marginBottom: 10
  }
});
