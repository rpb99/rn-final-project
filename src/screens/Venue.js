import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  ScrollView
} from "react-native";
import Axios from "axios";
class Venue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      venue: {},
      bookings: []
    };
  }
  getVenue() {
    let { id, token } = this.props.route.params;
    // console.log(id);
    Axios({
      method: "GET",
      url: `https://mainbersama.demosanbercode.com/api/venues/${id}`,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        this.setState({ venue: res.data.venue });
      })
      .catch(err => console.log(err.message));
  }
  getBooking() {
    let { id, token } = this.props.route.params;
    // console.log(id);
    Axios({
      method: "GET",
      url: `https://mainbersama.demosanbercode.com/api/bookings`,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      },
      params: {
        venue_id: id
      }
    })
      .then(res => {
        this.setState({ bookings: res.data.bookings });
      })
      .catch(err => console.log(err.message));
  }
  componentDidMount() {
    this.getVenue();
    this.getBooking();
  }
  render() {
    return (
      <View style={styles.screen}>
        <ScrollView contentContainerStyle={styles.container}>
          <View
            style={{
              marginTop: 10,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              style={{
                width: "90%",
                height: 250,
                backgroundColor: "black",
                borderRadius: 12,
                resizeMode: "contain"
              }}
              source={{
                uri:
                  "https://mainbersama.demosanbercode.com" +
                  this.state.venue.thumbnail
              }}
            />
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.textVenue}>{this.state.venue.name}</Text>
            <Text style={styles.kategoriText}>{this.state.venue.location}</Text>
            <Text style={styles.kategoriText}>
              Buka jam {this.state.venue.open_hour}
            </Text>
            <Text style={styles.kategoriText}>Jadwal Main Bareng:</Text>
            <ScrollView contentContainerStyle={styles.cardVenueScroll}>
              {this.state.bookings.map((e, i) => {
                return (
                  <View style={styles.cardVenue} key={e.id}>
                    <Image
                      source={{
                        uri:
                          "https://mainbersama.demosanbercode.com" +
                          this.state.venue.thumbnail
                      }}
                      style={{
                        height: "100%",
                        width: "50%",
                        resizeMode: "contain",
                        backgroundColor: "#ffffff",
                        borderRadius: 16,
                        flexDirection: "row"
                      }}
                    />
                    <View style={{ flexDirection: "column", marginLeft: 15 }}>
                      <Text style={{ color: "#0A3D62", fontSize: 20 }}>
                        {e.play_date}
                      </Text>
                      <Text style={{ color: "#0A3D62", marginBottom: 5 }}>
                        Jumlah Pemain: {e.total_players}
                      </Text>
                      <Button
                        title="Join Us"
                        color="#F6B93B"
                        onPress={() =>
                          this.props.navigation.navigate("Join", {
                            data: e,
                            token: this.props.route.params.token
                          })
                        }
                      />
                    </View>
                  </View>
                );
              })}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Venue;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff"
  },
  searchRow: {
    alignItems: "center"
  },
  kategoriText: {
    color: "#0A3D62",
    marginLeft: 20
  },
  cardKategoriScroll: {
    marginLeft: 10
  },
  cardKategori: {
    flexDirection: "column",
    alignContent: "center",
    alignItems: "center",
    padding: 15,
    margin: 5,
    height: 100,
    width: 100,
    backgroundColor: "#273C75",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  textVenue: {
    fontSize: 20,
    color: "#0A3D62",
    marginLeft: 20
  },
  cardKategoriText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  },
  cardVenueScroll: {
    marginHorizontal: 10,
    alignItems: "center",
    flexDirection: "column"
  },
  cardVenue: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    height: 100,
    width: "100%",
    backgroundColor: "#F5F6FA",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardVenueText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  }
});
