import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    FlatList,
    Button,
    TouchableOpacity
} from "react-native";
import { MaterialIcons, FontAwesome } from "@expo/vector-icons";

const colors = {
    white: "#FFFFFF",
    blue: "#5AB6FD"
};

const dummyData = [
    {
        id: "1"
    },
    {
        id: "2"
    },
    {
        id: "3"
    }
];

export default function App() {
    return (
        <View style={styles.screen}>
            <Text style={styles.titleText}>AYO Sedekah</Text>
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.componentContainer}>
                    <Text style={styles.headerText}>Informasi AYO Sedekah</Text>

                    <FlatList
                        horizontal={true}
                        data={dummyData}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => (
                            <View style={styles.infoContainer}>
                                <Text>Id ke-{item.id}</Text>
                            </View>
                        )}
                    />

                    <Text style={styles.infoLink}>
                        Lihat Informasi Ayo Sedekah
                    </Text>
                </View>

                <View style={styles.componentContainer}>
                    <Text style={styles.headerText}>Tambah Target Sedekah</Text>

                    <TouchableOpacity style={styles.buttonContainer}>
                        <MaterialIcons
                            name="person-add"
                            size={72}
                            color={colors.white}
                        />
                        <Text style={styles.buttonText}>
                            Tambah{"\n"}Sedekah
                        </Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.componentContainer}>
                    <Text style={styles.headerText}>Target Sedekah Anda</Text>

                    <View style={styles.targetContainer}>
                        <View style={styles.targetItemContainer}>
                            <View style={styles.imageContainer}>
                                <MaterialIcons
                                    name="person"
                                    size={40}
                                    color={colors.white}
                                />
                            </View>
                            <View>
                                <Text>Nama: Fulan</Text>
                                <View style={styles.categoryWrapper}>
                                    <Text>Kategori:</Text>
                                    <View style={styles.categoryContainer}>
                                        <Text style={styles.categoryText}>
                                            Anak Yatim
                                        </Text>
                                    </View>
                                </View>
                                <Text>
                                    Komitmen Sedekah Anda: Rp. 500.000,-
                                </Text>
                            </View>
                        </View>

                        <View style={styles.targetItemContainer}>
                            <View style={styles.imageContainer}>
                                <MaterialIcons
                                    name="person"
                                    size={40}
                                    color={colors.white}
                                />
                            </View>
                            <View>
                                <Text>Nama: Fulan</Text>
                                <View style={styles.categoryWrapper}>
                                    <Text>Kategori:</Text>
                                    <View style={styles.categoryContainer}>
                                        <Text style={styles.categoryText}>
                                            Dhuafa
                                        </Text>
                                    </View>
                                </View>
                                <Text>
                                    Komitmen Sedekah Anda: Rp. 500.000,-
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={styles.componentContainer}>
                    <Text style={styles.headerText}>History Sedekah Anda</Text>
                    <View style={styles.dummyComponent} />
                </View>

                <View style={styles.componentContainer}>
                    <Text style={styles.headerText}>Status Sedekah Anda</Text>
                    <View style={styles.dummyComponent} />
                </View>
            </ScrollView>

            <View style={styles.fabWrapper}>
                <TouchableOpacity onPress={() => {}}>
                    <View style={styles.fabContainer}>
                        <FontAwesome
                            name="handshake-o"
                            size={15}
                            color={colors.blue}
                        />
                    </View>
                    <Text style={styles.fabText}>Sedekah{"\n"}Sekarang</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        marginTop: 40
    },
    container: {
        marginHorizontal: 10,
        paddingBottom: 40
    },
    titleText: {
        textAlign: "center",
        color: "#FFB90F",
        fontSize: 24
    },
    componentContainer: {
        marginTop: 10
    },
    headerText: {
        marginLeft: 10,
        fontSize: 16,
        fontWeight: "bold"
    },
    infoContainer: {
        width: 150,
        height: 100,
        borderRadius: 10,
        backgroundColor: "grey",
        marginRight: 10
    },
    infoLink: {
        textAlign: "center",
        color: colors.blue
    },
    buttonContainer: {
        flexDirection: "row",
        alignItems: "center",
        padding: 5,
        backgroundColor: colors.blue,
        borderRadius: 10,
        alignSelf: "center"
    },
    buttonText: {
        color: "#FFFFFF",
        fontSize: 24,
        marginLeft: 4
    },
    targetContainer: {
        backgroundColor: "#e5e5e5",
        marginHorizontal: 10,
        padding: 10,
        borderRadius: 10
    },
    targetItemContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginVertical: 5
    },
    imageContainer: {
        width: 50,
        height: 50,
        backgroundColor: "grey",
        borderRadius: 10,
        marginRight: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    categoryWrapper: {
        flexDirection: "row",
        alignItems: "center"
    },
    categoryContainer: {
        backgroundColor: colors.blue,
        padding: 4,
        borderRadius: 10
    },
    categoryText: {
        color: "white"
    },
    dummyComponent: {
        marginVertical: 10,
        height: 200,
        width: "97%",
        backgroundColor: "grey",
        borderRadius: 10,
        alignSelf: "center"
    },
    fabWrapper: {
        position: "absolute",
        right: 15,
        bottom: 20
    },
    fabContainer: {
        backgroundColor: "white",
        height: 40,
        width: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: colors.blue,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center"
    },
    fabText: {
        color: colors.blue
    }
});
