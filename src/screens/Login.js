import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage
} from "react-native";
import { connect } from "react-redux";
import Logo from "../../assets/sport.png";

import { saveToken, login } from "../redux/actions";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: "",
      email: "",
      password: ""
    };
  }

  handleInput = data => {
    let obj = {
      [data.type]: data.text
    };
    this.setState(obj);
  };

  storeData = async value => {
    try {
      await AsyncStorage.setItem("token", value);
    } catch (e) {
      // saving error
    }
  };

  handleLogin = () => {
    // console.log('sudah login', this.state.accessToken)
    // this.props.navigation.navigate('Dashboard', {token: this.state.accessToken})

    this.props.login(this.state);
  };

  componentDidUpdate() {
    console.log("accessToken", this.state.accessToken);
  }
  /* 2. Get the param */
  // const { itemId } = route.params;
  // const { otherParam } = route.params;
  async componentDidUpdate(prevProps) {
    if (this.props.auth.token !== prevProps.auth.token) {
      this.storeData(this.props.auth.token);
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>Main Bersama</Text>
        <Image source={Logo} style={{ height: "25%", width: "80%" }} />
        <View style={styles.inputView}>
          <TextInput
            style={styles.inputText}
            placeholder="Username"
            placeholderTextColor="#718093"
            onChangeText={text => this.handleInput({ type: "email", text })}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password"
            placeholderTextColor="#718093"
            onChangeText={text => this.handleInput({ type: "password", text })}
          />
        </View>
        <TouchableOpacity
          style={styles.loginBtn}
          onPress={() => this.handleLogin()}
        >
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.signupBtn}
          onPress={() => this.props.navigation.navigate("Register")}
        >
          <Text style={styles.loginText}>SIGN UP</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    auth: state.authReducers
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token)),
    login: data => dispatch(login(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    fontWeight: "bold",
    fontSize: 40,
    color: "#fb5b5a",
    marginBottom: 5
  },
  inputView: {
    width: "80%",
    backgroundColor: "#F5F6FA",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowOffset: { height: 4, width: 4 }
  },
  inputText: {
    height: 50,
    color: "#718093"
  },
  forgot: {
    color: "#718093",
    fontSize: 11
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#273C75",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText: {
    color: "white"
  },
  signupBtn: {
    width: "80%",
    backgroundColor: "#F6B93B",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10
  }
});
