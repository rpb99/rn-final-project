import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  Alert,
  ScrollView
} from "react-native";
import Logo from "../../assets/sport.png";

export default class Join extends React.Component {
  render() {
    console.log(this.props.route.params.data);
    let {
      id,
      play_date,
      start,
      total_players,
      players_count
    } = this.props.route.params.data;
    let {
      location,
      name,
      thumbnail,
      open_hour
    } = this.props.route.params.data.venue;
    return (
      <View style={styles.screen}>
        <ScrollView contentContainerStyle={styles.container}>
          <View
            style={{
              marginTop: 10,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              style={{
                width: "90%",
                height: 250,
                backgroundColor: "black",
                borderRadius: 12,
                resizeMode: "contain"
              }}
              source={{
                uri: "https://mainbersama.demosanbercode.com" + thumbnail
              }}
            />
          </View>
          <View style={{ marginTop: 10 }}>
            {/* <Text style={styles.kategoriText}>Jadwal Main Bareng</Text> */}
            <ScrollView contentContainerStyle={styles.cardVenueScroll}>
              <View style={styles.cardVenue}>
                <View style={{ flexDirection: "column", marginLeft: 15 }}>
                  <Text style={{ color: "#0A3D62", fontSize: 20 }}>{name}</Text>
                  <Text style={{ color: "#0A3D62", marginBottom: 5 }}>
                    {location}
                  </Text>
                  <Text style={{ color: "#0A3D62", marginBottom: 5 }}>
                    Main Tanggal {play_date} jam {start}
                  </Text>
                  <Text style={{ color: "#0A3D62", marginBottom: 5 }}>
                    Jumlah Pemain: {total_players}
                  </Text>
                </View>
              </View>
              <Button
                title="Main Bareng"
                color="#F6B93B"
                onPress={() => Alert.alert("Kamu Berhasil Bergabung")}
              />
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff"
  },
  searchRow: {
    alignItems: "center"
  },
  kategoriText: {
    color: "#0A3D62",
    marginLeft: 20
  },
  cardKategoriScroll: {
    marginLeft: 10
  },
  cardKategori: {
    flexDirection: "column",
    alignContent: "center",
    alignItems: "center",
    padding: 15,
    margin: 5,
    height: 100,
    width: 100,
    backgroundColor: "#273C75",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardKategoriText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  },
  cardVenueScroll: {
    marginHorizontal: 10,
    alignItems: "center",
    flexDirection: "column"
  },
  cardVenue: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    height: 100,
    width: "100%",
    backgroundColor: "#F5F6FA",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardVenueText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  }
});
