import "react-native-gesture-handler";
import React from "react";
import { AsyncStorage } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { connect } from "react-redux";

import { Login, Register, Dashboard, Venue, Join } from "../Index";

import { saveToken } from "../redux/actions";

const Stack = createStackNavigator();

class AppNavigation extends React.Component {
  checkToken = async () => {
    try {
      let token = await AsyncStorage.getItem("token");
      return token === null ? null : token;
    } catch (error) {
      return null;
    }
  };

  async componentDidMount() {
    let token = await this.checkToken();
    token !== null;
    token ? this.props.saveToken(token) : this.props.saveToken("");
  }

  render() {
    // console.log("Auth prop", this.props.auth);
    const token = this.props.auth.token;
    return token === "" ? (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Register"
            component={Register}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    ) : (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="Venue" component={Venue} />
          <Stack.Screen
            name="Join"
            component={Join}
            options={{ title: "Main Bareng" }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
// subscribe ke store

const mapStateToProps = state => {
  return {
    auth: state.authReducers
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppNavigation);
