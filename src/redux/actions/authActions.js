import {
  SAVE_TOKEN,
  LOGIN_REQUEST,
  LOGIN_ERROR,
  LOGIN_SUCCESS
} from "../actionTypes";
import Axios from "axios";
import { baseUrl } from "../../constants";

export const saveToken = token => ({ type: SAVE_TOKEN, payload: token });

export const login = data => {
  return (dispatch, getState) => {
    dispatch({ type: LOGIN_REQUEST });
    Axios({
      method: "POST",
      url: `${baseUrl}/login`,
      headers: {
        Accept: "application/json"
        // Authorization: `Bearer ${getstate().authReducers.token}`
      },
      data: data
    })
      .then(({ data }) => {
        console.log("respon login", data);
        // success
        if (data.success) {
          dispatch({ type: LOGIN_SUCCESS, payload: data.token });
        } else {
          dispatch({ type: LOGIN_ERROR, payload: data.message });
        }
      })
      .catch(err => {
        dispatch({ type: LOGIN_ERROR, payload: err.message });
      });
  };
};
export const register = data => {
  return (dispatch, getState) => {
    dispatch({ type: "REGISTER_REQUEST" });
    Axios({
      method: "POST",
      url: `${baseUrl}/register`,
      headers: {
        Accept: "application/json"
        // Authorization: `Bearer ${getstate().authReducers.token}`
      },
      data: data
    })
      .then(({ data }) => {
        console.log("respon login", data);
      })
      .catch(err => {
        dispatch({ type: LOGIN_ERROR, payload: err.message });
      });
  };
};
